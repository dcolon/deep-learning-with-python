# Deep learning with python

** Daniel Colon - 27-10-2022**

This repository contains jupyter lab notebooks covering the various chapters from the book [Deep learning with python](https://www.manning.com/books/deep-learning-with-python)
