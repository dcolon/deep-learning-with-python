# Define a bunch of useful helper functions.

import matplotlib.pyplot as plt

def plot_acc(history):
    history_dict = history.history
    acc_values = history_dict['accuracy']
    val_acc_values = history_dict['val_accuracy']

    plt.plot(epochs, acc_values, 'bo', label = 'Training accuracy') # bo is blue dot.
    plt.plot(epochs, val_acc_values, 'b', label = 'Validation accuracy') # b is blue line.
    plt.title('Training and validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()

    plt.show()

def plot_loss(history):
    history_dict = history.history
    loss_values = history_dict['loss']
    val_loss_values = history_dict['val_loss']

    epochs = range(1, len(loss_values) + 1)

    plt.plot(epochs, loss_values, 'bo', label = 'Training loss') # bo is blue dot.
    plt.plot(epochs, val_loss_values, 'b', label = 'Validation loss') # b is blue line.
    plt.title('Training and validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()

    plt.show()

def plot_average_mae_history(average_mae_history):
    plt.plot(range(1, len(average_mae_history) + 1), average_mae_history, 'b', label = 'MAE')
    plt.title('Average mae history')
    plt.xlabel('Epochs')
    plt.ylabel('Validation MAE')
    plt.legend()

    plt.show()

def smooth_curve(points, factor = 0.9):
    smoothed_points = []
    for point in points:
        if smoothed_points:
            previous = smoothed_points[-1]
            smoothed_points.append(previous * factor + point * (1 - factor))
        else:
            smoothed_points.append(point)
    return smoothed_points